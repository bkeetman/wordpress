<?php

register_nav_menus( array(
    'primary' => 'Primary Navigation'
) );

add_filter('timber_context', 'add_to_context');
function add_to_context($data)
{
    $data['menu'] = new TimberMenu(); // This is where you can also send a Wordpress menu slug or ID
    return $data;
}

if ( function_exists( 'add_theme_support' ) )
{
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 1200, 600, true );
}

/*
 * Appearance permissions: edit Appearance menu for selected roles
 * Author: Vladimir Garagulya
 * email: support@role-editor.com
 *
 */

class AppearancePermissions {

    // roles to which allow the Appearance menu
    private $allowed_roles = array('editor');
    // allowed items from Appearance menu
    private $allowed_items = array(
        'widgets.php',
        'nav-menus.php'
    );
    // capabilities required by WordPress to access to the menu items above
    private $required_capabilities = array(
      'edit_theme_options'
    );

    private $prohibited_items = array();


    public function __construct() {

        add_filter('user_has_cap', array($this, 'add_required_caps'), 10, 4);
        add_action('admin_head', array($this, 'remove_appearance_menu_items'), 10);
        add_action('admin_head', array($this, 'appearance_redirect'), 10);
    }


    protected function user_has_allowed_role($user) {
        foreach($user->roles as $role) {
            if (in_array($role, $this->allowed_roles)) {
                return true;
            }
        }

        return false;
    }
    // end of user_has_allowed_role()


    public function add_required_caps($allcaps, $caps, $args, $user) {
        if (!$this->user_has_allowed_role($user)) {
            return $allcaps;
        }

        remove_filter('user_has_cap', array($this, 'add_required_caps'), 10, 4);
        foreach($this->required_capabilities as $cap) {
            if (!$user->has_cap($cap)) {
                $allcaps[$cap] = true;
            }
        }
        add_filter('user_has_cap', array($this, 'add_required_caps'), 10, 4);

        return $allcaps;
    }
    // end of add_required_capabilities()



    public function remove_appearance_menu_items() {
      global $current_user, $submenu;

      if (!$this->user_has_allowed_role($current_user)) {
          return;
      }

      if (!isset($submenu['themes.php'])) {
          return;
      }
      // remove not allowed menu items
      foreach($submenu['themes.php'] as $key=>$item) {
          if (!in_array($item[2], $this->allowed_items)) {
              $this->prohibited_items[] = $item[2];
              unset($submenu['themes.php'][$key]);
          }
      }
    }
    // end of remove_appearance_menu_items()


    public function appearance_redirect() {
        global $current_user;

        if (!$this->user_has_allowed_role($current_user)) {
          return;
        }

        foreach ($this->prohibited_items as $item) {
            $result = stripos($_SERVER['REQUEST_URI'], $item);
            if ($result !== false) {
                wp_redirect(get_option('siteurl') . '/wp-admin/index.php');
            }
            break;
        }
    }
    // end of appearance_redirect()

}
// end of class AppearancePermissions()


new AppearancePermissions();