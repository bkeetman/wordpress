<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
if ( is_dev() )
{
    define('DB_NAME', 'wordpress');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'root123');
    define('DB_HOST', 'localhost');
}
else
{
    define('DB_NAME', 'wordpress');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'root123');
    define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content/' );
// define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp' );

define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );
define( 'WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hx2NjZhMERwY`T#|]g3[L|@Qa/XynG,r|}vd]}y[4CeFO(|HvD2 r:ZCgoyFe?sv');
define('SECURE_AUTH_KEY',  '}-;!DVff(hwhD=,H=Jhf #lB$P26u 8R:;@6eCW^Iu/8NaR|Pi=9mj>8Q^Qk9.o^');
define('LOGGED_IN_KEY',    'pz(xj;Y+68NJ<!PEBIK2ABm^UF!/)bb&[h~c,+m-IoE!UXa0pnA>^;,Q,iW=Q(UF');
define('NONCE_KEY',        'MgjXL!&}$VU$r7Hm]Z(%dwj-hr!n.CW;a#P:My-9|ZpeGl0<L]/3]. I+U}}E-i-');
define('AUTH_SALT',        '#Ox~PBAgcxf}$TsV+fn?I+`2S2q.4uU<jW3J!b<iQ!Y~L!~&E$|IYBG^3chSwc@h');
define('SECURE_AUTH_SALT', '25D|?y0Q={@O,^m#>~Mx3iw@IsXIsk^XJS]-t`w79YCKCh>J)l,=>h(2EeBZLUv0');
define('LOGGED_IN_SALT',   'RU%uhfi#hOHCoj,&^@%&+a/1i-MWrK|iWrn_4Z_1;X@C?q5j9{z|S7Vg(knj]+?/');
define('NONCE_SALT',       '+_EKNpZttjCaI^UfoS<s=N!yqigt=:W*guCqr6+:HLisOlD$px|Ckfwqmb1RFx]q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'nl');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

function is_dev() {

    $info = pathinfo($_SERVER['SERVER_NAME']);
    if( $info['extension'] == 'dev' ) {
        return true;
    }

    return false;
}