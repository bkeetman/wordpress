var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    sass        = require('gulp-sass'),
    minifyCSS   = require('gulp-minify-css'),
    uglify      = require('gulp-uglify'),
    rename      = require('gulp-rename'),
    watch       = require('gulp-watch'),
    notify      = require('gulp-notify');

var src = {
  scss: 'wp-content/themes/default/assets/scss/main.scss'
};

var dest = {
    css: 'wp-content/themes/default/assets/css/'
};

gulp.task('sass', function () {
    gulp.src(src.scss)
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest(dest.css))
        .pipe(notify("Generated sass"));
});

gulp.task('watch', function() {
    gulp.watch(src.scss, ['sass']);
});
